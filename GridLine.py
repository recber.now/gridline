import utm
import math
from datetime import datetime

#lat_lt = 46.461489
#lon_lt = 42.572881
#lat_rb = 46.400202
#lon_rb = 42.773038
#grid_lenght = 2000
#file_name = 'Izmailovskii'

grid = []
track = []
alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z",
            "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR",
            "AS", "AT", "AU",
            "AV", "AW", "AX", "AY", "AZ"]


def calc(self):
    date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    rectangle = [max([lat_lt, lat_rb]), min([lon_lt, lon_rb]), min([lat_lt, lat_rb]), max([lon_lt, lon_rb])]
    utm_lt = utm.from_latlon(rectangle[0], rectangle[1])
    utm_rb = utm.from_latlon(rectangle[2], rectangle[3])
    utm_zone = [utm_lt[2], utm_lt[3]]
    utm_east = utm_rb[0] - utm_lt[0]
    utm_north = utm_lt[1] - utm_rb[1]
    count_east = math.ceil(utm_east / grid_lenght) + 2
    if count_east / 2 == round(count_east / 2):
        count_east = count_east + 1
    count_north = math.ceil(utm_north / grid_lenght) + 2
    if count_north / 2 == round(count_north / 2):
        count_north = count_north + 1
    first_point_east = math.floor(utm_lt[0] / grid_lenght) * grid_lenght
    first_point_north = math.ceil(utm_lt[1] / grid_lenght) * grid_lenght
    for i in range(count_north):
        for j in range(count_east):
            point = utm.to_latlon(first_point_east + grid_lenght * j, first_point_north - grid_lenght * i, utm_zone[0],
                                  utm_zone[1])
            grid.append(
                [alphabet[j] + str(i + 1), round(point[0], 6), round(point[1], 6), first_point_east + grid_lenght * j,
                 first_point_north - grid_lenght * i])
    track.append([grid[0][0], grid[0][1], grid[0][2]])

    for k, l in enumerate(grid):
        if k / count_east == round(k / count_east):
            if k / 2 != round(k / 2):
                track.append([grid[k][0], grid[k][1], grid[k][2]])
                track.append([grid[k + count_east - 1][0], grid[k + count_east - 1][1], grid[k + count_east - 1][2]])
            else:
                track.append([grid[k + count_east - 1][0], grid[k + count_east - 1][1], grid[k + count_east - 1][2]])
                track.append([grid[k][0], grid[k][1], grid[k][2]])

    for k, l in enumerate(grid):
        if k <= count_east:
            if k / 2 == round(k / 2):
                if count_east / 2 == round(count_east / 2) and k == count_east:
                    pass
                else:
                    track.append([grid[k][0], grid[k][1], grid[k][2]])
                    track.append(
                        [grid[k + (count_north - 1) * count_east][0], grid[k + (count_north - 1) * count_east][1],
                         grid[k + (count_north - 1) * count_east][2]])
            else:
                if count_east / 2 != round(count_east / 2) and k == count_east:
                    pass
                else:
                    track.append(
                        [grid[k + (count_north - 1) * count_east][0], grid[k + (count_north - 1) * count_east][1],
                         grid[k + (count_north - 1) * count_east][2]])
                    track.append([grid[k][0], grid[k][1], grid[k][2]])

    filename = datetime.now().strftime("%Y-%m-%d") + "_" + self + "_" + str(grid_lenght) + "m" + ".gpx"

    with open(filename, 'w') as out:
        out.write("""<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <gpx
    xmlns="http://www.topografix.com/GPX/1/1"
    creator="GridLine.py"
    version="1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    """)
        out.write("<metadata>\n")
        out.write("<time>" + date + "</time>\n")
        out.write("<bounds minlat=\"" + str(lat_rb) + "\" minlon=\"" + str(lon_lt) + "\" maxlat=\"" + str(
            lat_lt) + "\" maxlon=\"" + str(lon_rb) + "\"/>\n")
        out.write("</metadata>\n")
        for m, n in enumerate(grid):
            out.write("<wpt lat=\"" + str(grid[m][1]) + "\" lon=\"" + str(grid[m][2]) + "\">\n")
            out.write("<time>" + date + "</time>\n")
            out.write("<name>" + str(grid[m][0]) + "</name>\n")
            out.write("</wpt>\n")

        out.write("<trk>\n")
        out.write("<name>Grid_Line</name>\n")
        out.write("<desc></desc>\n")
        out.write("<trkseg>\n")
        for o, p in enumerate(track):
            out.write("<trkpt lat=\"" + str(track[o][1]) + "\" lon=\"" + str(track[o][2]) + "\">\n")
            out.write("<time>" + date + "</time>\n")
            out.write("</trkpt>\n")
        out.write("</trkseg>\n")
        out.write("</trk>\n")
        out.write("</gpx>")


if __name__ == "__main__":
    lat_lt = float(input ("First point latitude:"))
    lon_lt = float(input ("First point longitude:"))
    lat_rb = float(input ("Second point latitude:"))
    lon_rb = float(input ("Second point longitude:"))
    grid_lenght = int(input ("Cell lenght:"))
    file_name = input("FileName:")

    if grid_lenght / 500 != round(grid_lenght / 500):
        print("Please correct your input data")
    else:
        calc(file_name)
